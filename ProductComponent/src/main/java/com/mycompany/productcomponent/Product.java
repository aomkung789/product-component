package com.mycompany.productcomponent;

import java.util.ArrayList;


public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"Eppresso 1",40,"Eppresso.png"));
        list.add(new Product(1,"Americano",45,"Americano.png"));
        list.add(new Product(1,"Cocoa",30,"Cocoa.png"));
        list.add(new Product(1,"Cappuchino",35,"Cappuchino.png"));
        list.add(new Product(1,"Latte",40,"Latte.png"));
        list.add(new Product(1,"Matcha",45,"Matcha.png"));
        list.add(new Product(1,"Thaitea",50,"Thaitea.png"));
        return list;
    }
}
